package com.barnes.data.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;
import java.util.UUID;

@RestController
public class UserController
{
    @Autowired
    private UserRepository userRepository;

    @GetMapping( "/test" )
    @ResponseStatus( HttpStatus.OK )
    public String test()
    {
        return "ok";
    }
    
    @GetMapping( "/{id}" )
    @ResponseStatus( HttpStatus.OK )
    public User getUser(  @PathVariable( "id" ) User user )
    {
        return user;
    }
    
    @GetMapping( "/listAll" )
    @ResponseStatus( HttpStatus.OK )
    public List<User> listAll()
    {
        return userRepository.listAll();
    }
}


