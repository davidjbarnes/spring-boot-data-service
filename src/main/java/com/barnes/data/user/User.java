package com.barnes.data.user;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Embeddable
@Table( schema = "user_ds", name = "user" )
public class User
{
    @Id
    @Column( name = "id" )
    private UUID id;
    
    @Column( name = "username" )
    private String username;
    
    @Column( name = "password" )
    private String password;

    User()
    {
    }
    
    public UUID getId()
    {
        return id;
    }
    
    public String getUsername()
    {
        return username;
    }
    
    public String getPassword()
    {
        return password;
    }
}
