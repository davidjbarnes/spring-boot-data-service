package com.barnes.data.user;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends PagingAndSortingRepository<User, UUID>
{	
	@Query("select u from User u")
    List<User> listAll();
}